import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import eventsKeys from '../../mocks/systems/constants/eventsKeys';


const systemToObservable = systemEventTarget => fromEvent(systemEventTarget, eventsKeys.systemDataEvent)
  .pipe(map(({ detail }) => {
    if (!detail || detail.value === undefined) {
      return null
    }
    return detail.value;
  }));

export default systemToObservable;
