import { Observable } from 'rxjs';
import DataEvent from '../../mocks/systems/events/DataEvent';
import systemToObservable from './index';


describe('systemToObservable bridge test', () => {
  const systemMock = new EventTarget();
  let systemObservable;

  beforeEach(() => {
    systemObservable = systemToObservable(systemMock);
  })

  test('Should convert EventTarget to Observable', () => {
    expect(systemObservable)
      .toBeInstanceOf(Observable);
  });

  test('Observable should handle data event with correct payload', () => {
    const callback = jest.fn();

    systemObservable.subscribe(callback);

    systemMock.dispatchEvent(new DataEvent({ value: 10 }));

    expect(callback.mock.calls.length)
      .toBe(1);

    expect(callback.mock.calls[0][0])
      .toBe(10);
  });

  test(
    'Observable should handle data event with incorrect payload and return `null` ',
    () => {
      const callback = jest.fn();

      systemObservable.subscribe(callback);

      systemMock.dispatchEvent(new DataEvent());

      expect(callback.mock.calls.length)
        .toBe(1);

      expect(callback.mock.calls[0][0])
        .toBe(null);
    });

  test(
    'Observable should return value event if it can be converted to false',
    () => {
      const callback = jest.fn();

      systemObservable.subscribe(callback);

      systemMock.dispatchEvent(new DataEvent({ value: 0 }));

      expect(callback.mock.calls.length)
        .toBe(1);

      expect(callback.mock.calls[0][0])
        .toBe(0);
    });
});
