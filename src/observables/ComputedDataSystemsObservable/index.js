import { combineLatest } from 'rxjs';
import { throttleTime, map } from 'rxjs/operators';
import appConfig from '../../appConfig';
import wrapSystemObservable from './helpers/wrapSystemObservable';


/**
 * Created for incapsulate critical class properties
 *
 * @type {symbol}
 */
const systemsSymbol = Symbol('systems list');
const observerSymbol = Symbol('link to observable instance');

class ComputedDataSystemsObservable {
  [systemsSymbol] = new Map();
  [observerSymbol];

  /**
   * Expect object for keep initial keys
   *
   * @param {Object<Observable>} dataSystems
   */
  constructor(dataSystems) {
    if (!dataSystems
      || typeof dataSystems !== 'object'
      || Array.isArray(dataSystems)
      || !Object.keys(dataSystems).length
    ) {
      throw new Error('Expected at least one system for subscribe as {[key]: systemObservable}');
    }

    this[systemsSymbol] = new Map(Object.entries(dataSystems));

    this.generateObservable();
  }

  generateObservable() {
    const systemsKeys = [...this[systemsSymbol].keys()];
    const systems = [...this[systemsSymbol].values()];

    this[observerSymbol] = combineLatest(
      /**
       * loop through system providers (EventTargets) and convert them to Observable
       * Also apply required pipes and checks
       */
      systems.map(systemObservable => wrapSystemObservable(systemObservable)),
    )
      .pipe(throttleTime(appConfig.observerThrottleTime))
      /**
       * Pipe for convert array result to object with bindings to initial keys
       */
      .pipe(map((results) => {
        return results.reduce(
          (accum, value, index) => {
            return { ...accum, [systemsKeys[index]]: value }
          },
          {},
        )
      }));
  }

  /**
   * @param {function} callback
   * @returns {{unsubscribe: function}}
   */
  subscribe(callback) {
    if (typeof callback !== 'function') {
      throw new Error(`Expected function but got ${typeof callback}`)
    }

    return this[observerSymbol].subscribe(callback);
  }
}

export default ComputedDataSystemsObservable;
