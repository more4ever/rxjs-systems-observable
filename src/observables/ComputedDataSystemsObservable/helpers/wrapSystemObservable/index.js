import { merge } from 'rxjs';
import { debounceTime, mapTo } from 'rxjs/operators';
import appConfig from '../../../../appConfig';


/**
 * Wrap event target with Observable and subscribe to 'data' event
 *
 * @param {Observable} systemObservable
 * @returns {Observable<number|string>}
 */
const wrapSystemObservable = (systemObservable) => {
  /**
   * Observable for set default value if system idle more than value from config
   * @type {Observable<string>}
   */
  const naObservable = systemObservable
    .pipe(debounceTime(appConfig.observerDefaultValueEmitTime), mapTo('N/A'));

  return merge(systemObservable, naObservable);
}

export default wrapSystemObservable;
