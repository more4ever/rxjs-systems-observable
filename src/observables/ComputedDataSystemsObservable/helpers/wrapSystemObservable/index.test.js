import { Observable } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import appConfig from '../../../../appConfig';
import wrapSystemObservable from './index';


describe(
  'ComputedDataSystemsObservable EventTarget wrapper tests',
  function () {
    let testScheduler;

    beforeEach(() => {
      testScheduler = new TestScheduler(
        (actual, expected) => expect(actual)
          .toEqual(expected),
      );
    })

    test('Should return observable', () => {
      testScheduler.run(({ cold }) => {

        const wrappedSystemObservable = wrapSystemObservable(cold('a'));

        expect(wrappedSystemObservable)
          .toBeInstanceOf(Observable);
      });
    })

    test(
      'Should handle data event with correct payload ',
      () => {
        testScheduler.run(({ expectObservable, cold }) => {
          const mock = cold('a', { a: 1 });

          const wrappedSystemObservable = wrapSystemObservable(mock);

          expectObservable(wrappedSystemObservable, '-!')
            .toBe('a', { a: 1 })
        })
      },
    );

    test(
      `Should handle value and trigger N/A after ${appConfig.observerDefaultValueEmitTime}ms`,
      () => {
        testScheduler.run(({ expectObservable, cold }) => {
          const mock = cold('a', { a: 1 });

          const wrappedSystemObservable = wrapSystemObservable(mock);
          /**
           * `- 1` - because a event is first frame
           */
          expectObservable(wrappedSystemObservable)
            .toBe(`a ${appConfig.observerDefaultValueEmitTime - 1}ms b`, { a: 1, b: 'N/A' });
        })
      },
    );

    test(
      `Should handle value twice with 500ms interval than trigger N/A after ${appConfig.observerDefaultValueEmitTime}ms`,
      () => {
        testScheduler.run(({ expectObservable, cold }) => {
          const mock = cold('a 500ms b', { a: 1, b: 2 });

          const wrappedSystemObservable = wrapSystemObservable(mock);
          /**
           * `- 1` - because a event is first frame
           */
          expectObservable(wrappedSystemObservable)
            .toBe(
              `a 500ms b ${appConfig.observerDefaultValueEmitTime - 1}ms c`,
              { a: 1, b: 2, c: 'N/A' },
            );
        })
      },
    );
  },
)



