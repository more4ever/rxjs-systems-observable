import { TestScheduler } from 'rxjs/testing';
import appConfig from '../../appConfig';
import wrapSystemObservable from './helpers/wrapSystemObservable';
import ComputedDataSystemsObservable from './index';


describe(
  'ComputedDataSystemsObservable test',
  () => {

    let testScheduler;

    beforeEach(() => {
      testScheduler = new TestScheduler(
        (actual, expected) => expect(actual)
          .toEqual(expected),
      );
    })

    test(
      'Should throw an error for empty incorrect dataSystems argument',
      () => {
        const expectFunction = () => {
          new ComputedDataSystemsObservable();
        }
        expect(expectFunction)
          .toThrow(Error);
      },
    );

    test(
      'Should throw an error for incorrect subscription argument',
      () => {
        const expectFunction = () => {
          testScheduler.run(({ cold }) => {
            const wrappedSystemObservable = wrapSystemObservable(cold(''));
            const computedSystemsObservable = new ComputedDataSystemsObservable({
              first: wrappedSystemObservable,
            })

            computedSystemsObservable.subscribe('test');
          });
        }

        expect(expectFunction)
          .toThrow(Error);
      },
    );

    test(
      'Should ignore single system change',
      () => {
        testScheduler.run(({ expectObservable, cold }) => {

          const firstWrappedSystemObservable = wrapSystemObservable(cold('a', { a: 1 }));
          const secondWrappedSystemObservable = wrapSystemObservable(cold(''));
          const computedSystemsObservable = new ComputedDataSystemsObservable({
            first: firstWrappedSystemObservable,
            second: secondWrappedSystemObservable,
          })

          expectObservable(computedSystemsObservable, '-!')
            .toBe('')
        });
      },
    );

    test(
      'Should trigger change after both system provide value',
      () => {
        testScheduler.run(({ expectObservable, cold }) => {

          const firstWrappedSystemObservable = wrapSystemObservable(cold('a', { a: 1 }));
          const secondWrappedSystemObservable = wrapSystemObservable(cold('a', { a: 2 }));
          const computedSystemsObservable = new ComputedDataSystemsObservable({
            first: firstWrappedSystemObservable,
            second: secondWrappedSystemObservable,
          })

          expectObservable(computedSystemsObservable, '-!')
            .toBe('a', { a: { first: 1, second: 2 } })
        });
      },
    );

    test(
      `Should trigger change no more than once in ${appConfig.observerThrottleTime}ms`,
      () => {
        testScheduler.run(({ expectObservable, cold }) => {
          const halfOfThrottle = Math.round(appConfig.observerThrottleTime / 2)

          const firstMock = cold('a', { a: 1 });
          /**
           * a - 0 frame
           * b - halfOfThrottle frame, should be skipped
           * c - appConfig.observerThrottleTime + 1 frame
           */
          const secondMock = cold(
            `a ${halfOfThrottle - 1}ms b ${appConfig.observerThrottleTime - halfOfThrottle}ms c`,
            { a: 1, b: 2, c: 3 },
          );

          const firstWrappedSystemObservable = wrapSystemObservable(firstMock);
          const secondWrappedSystemObservable = wrapSystemObservable(secondMock);

          const computedSystemsObservable = new ComputedDataSystemsObservable({
            first: firstWrappedSystemObservable,
            second: secondWrappedSystemObservable,
          })

          expectObservable(computedSystemsObservable, `- ${appConfig.observerThrottleTime + 100}ms !`)
            .toBe(
              `a ${appConfig.observerThrottleTime}ms b`,
              {
                a: { first: 1, second: 1 },
                b: { first: 1, second: 3 },
              },
            )
        });
      },
    );
  },
)
