/**
 * Isolated config values, for keep them in one place
 * @type {{observerThrottleTime: number, observerDefaultValueEmitTime: number}}
 */
const appConfig = Object.freeze({
  observerThrottleTime: 100,
  observerDefaultValueEmitTime: 1000,
});

export default appConfig;
