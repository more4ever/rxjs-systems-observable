import React, { useEffect, useState } from 'react';
import systemToObservable from './helpers/systemToObservable';
import AirPressureSystem from './mocks/systems/AirPressureSystem';
import HumiditySystem from './mocks/systems/HumiditySystem';
import TemperatureSystem from './mocks/systems/TemperatureSystem';
import ComputedDataSystemsObservable from './observables/ComputedDataSystemsObservable';


function App() {
  const [systemsResult, setSystemsResult] = useState(null)

  useEffect(
    () => {
      const systems = {
        airPressure: systemToObservable(new AirPressureSystem()),
        temperature: systemToObservable(new TemperatureSystem()),
        humidity: systemToObservable(new HumiditySystem()),
      }

      const subscription = (new ComputedDataSystemsObservable(systems))
        .subscribe(setSystemsResult);

      return () =>
        subscription.unsubscribe();
    },
    [],
  )

  if (!systemsResult) {
    return 'Waiting for data';
  }

  const keys = Object.keys(systemsResult)

  return (
    <div>
      <table border={1}>
        <thead>
          <tr>
            {keys.map((key) => (
              <th key={`head-cell-${key}`}>
                {key}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>

          <tr>
            {keys.map((key) => (
              <td key={`head-cell-${key}`}>
                {systemsResult[key]}
              </td>
            ))}
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
