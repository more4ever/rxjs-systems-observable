import random from 'lodash.random';

import NotImplementedError from '../../../errors/NotImplementedError';
import DataEvent from '../events/DataEvent';


function timer(ms) {
  return new Promise(res => setTimeout(res, ms));
}

class BaseDataSystem extends EventTarget {
  constructor() {
    super();
    this.scheduleDataEmit()
  }

  get dataValue() {
    throw new NotImplementedError();
  }

  async scheduleDataEmit() {
    // Loop will be destroyed with instance
    for (let i = 0; ; i++) {
      await timer(random(100, 2000))
      this.emitData();
    }
  }

  emitData() {
    this.dispatchEvent(new DataEvent({ value: this.dataValue }));
  }
}

export default BaseDataSystem;
