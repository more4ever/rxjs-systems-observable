import random from 'lodash.random';

import BaseDataSystem from './core/BaseDataSystem';


class AirPressureSystem extends BaseDataSystem {

  get dataValue() {
    return random(740, 770);
  }
}

export default AirPressureSystem;
