import eventsKeys from '../constants/eventsKeys';


class DataEvent extends CustomEvent {
  constructor(payload) {
    super(eventsKeys.systemDataEvent, { detail: payload });
  }
}

export default DataEvent;
