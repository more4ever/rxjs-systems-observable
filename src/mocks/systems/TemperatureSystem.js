import random from 'lodash.random';

import BaseDataSystem from './core/BaseDataSystem';


class TemperatureSystem extends BaseDataSystem {

  get dataValue() {
    return random(-20, 35);
  }
}

export default TemperatureSystem;
