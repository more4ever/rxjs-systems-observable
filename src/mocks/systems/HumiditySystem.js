import random from 'lodash.random';

import BaseDataSystem from './core/BaseDataSystem';


class HumiditySystem extends BaseDataSystem {

  get dataValue() {
    return random(50, 100);
  }
}

export default HumiditySystem;
