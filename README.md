This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

RxJS implementation of observable summary object from provided systems based on TargetEvent

#### Conditions:
- summary object should not be emitted more often than every 100ms
- summary object should only be emitted when one of the systems sends a new value
- summary object should be emitted only after all provided systems emit at least one value
- `N/A` - value should be emited after 1000ms of system idle time since last value event
- Coverage by unit tests

## Notice 

Project uses non default test environment `jsdom-fourteen` instead of `jsdom`, due to TargetEvents issues.
Keep this in mind when start tests with non default script.


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

